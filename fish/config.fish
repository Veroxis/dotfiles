############
# +------+ #
# | PATH | #
# +------+ #
############
set -p PATH ~/.bin
set -p PATH ~/.local/bin
set -p PATH ~/.local/share/bin
set -p PATH ~/.composer/vendor/bin
set -p PATH ~/.krew/bin
set -p PATH ~/.zvm/bin
set -p PATH ~/go/bin
set -p PATH ~/.brew/bin
set -p PATH ~/.cargo/bin

# <CTRL+Backspace>
bind \cH backward-kill-word
# <CTRL+Delete>
bind \e\[3\;5~ kill-word

#######################
# +-----------------+ #
# | starship prompt | #
# +-----------------+ #
#######################

if status --is-interactive && type -q starship
    source ("starship" init fish --print-full-init | psub)
end

#######################
# +-----------------+ #
# | zoxide autojump | #
# +-----------------+ #
#######################

if status --is-interactive && type -q zoxide
    # Autojump: https://github.com/ajeetdsouza/zoxide
    export _ZO_ECHO="1"
    zoxide init fish | source
end

################################
# +--------------------------+ #
# | fnm node version manager | #
# +--------------------------+ #
################################

if status --is-interactive && type -q fnm
    # fnm: https://github.com/Schniz/fnm
    fnm env --use-on-cd --resolve-engines --corepack-enabled --shell fish | source
end

###############
# +---------+ #
# | aliases | #
# +---------+ #
###############

alias ll="ls --color=auto --human-readable --indicator-style=slash --group-directories-first --almost-all -l"
alias ls="ls --color=auto"

if type -q bat
    alias bat="bat --theme Dracula --paging never";
end

if type -q mpv
    alias mpv="mpv --loop-file=inf";
end

if type -q jeeves
    alias gg='jeeves update-system'
end

if type -q kubectl
    alias k="kubectl"
    export KUBE_EDITOR="nvim"
    if type -q kubectx
        alias kctx="kubectx"
    end
    if type -q kubens
        alias kns="kubens"
    end
end

if type -q nvim
    alias vim="nvim"
    alias less="nvim -R -"
end
