import os
import sys
import logging
from pathlib import Path
from contextlib import ExitStack
from typing import List, Optional, Union

SCRIPT_PATH = os.path.abspath(os.path.dirname(__file__))
logging.basicConfig(level=logging.INFO, format='[%(levelname)s] - %(message)s')

class Symlink:
    def __init__(self, target: Path, source: Path):
        self.target = target
        self.source = source

    @staticmethod
    def format_symlink_string(target: Union[str, Path], source: Union[str, Path]) -> str:
        return f"'{target}' -> '{source}'"

    def install(self) -> None:
        def print_exit_message(target: Path, source: Path, success: bool, suppress: bool) -> None:
            if suppress == False:
                if success == True:
                    logging.info(f"Symlinking: {Symlink.format_symlink_string(target, source)}")
                else:
                    logging.warning(f"Failed to Symlink: {Symlink.format_symlink_string(target, source)}")

        with ExitStack() as stack:
            stack.callback(lambda: print_exit_message(self.target, self.source, success, suppress))
            success = False
            suppress = False
            target_basepath = Path(os.path.dirname(self.target))
            if not target_basepath.exists() or target_basepath.is_symlink():
                if target_basepath.is_symlink():
                    logging.warning(f"Cant create directory because there is an Symlink: '{target_basepath}'")
                    return
                if target_basepath.is_socket():
                    logging.warning(f"Cant create directory because there is an Socket: '{target_basepath}'")
                    return
                logging.info(f"Creating directory: '{target_basepath}'")
                target_basepath.mkdir(exist_ok=True)
            if self.target.exists() or self.target.is_symlink():
                match self.target.is_symlink():
                    case True:
                        match self.target.readlink() == self.source:
                            case True:
                                logging.info(f"Skipped Symlink(already exists): {Symlink.format_symlink_string(self.target, self.source)}")
                                success = True
                                suppress = True
                            case False:
                                logging.warning(f"Unexpected Symlink")
                                logging.warning(f"  expected: {Symlink.format_symlink_string(self.target, self.source)}")
                                logging.warning(f"  found: {Symlink.format_symlink_string(self.target, self.target.readlink())}")
                    case False:
                        logging.warning(f"Skipping Path as it already exists and is not a symlink: '{self.target}'")
                        suppress = True
                return
            self.target.symlink_to(self.source)
            success = True

    def delete(self) -> None:
        match self.target.is_symlink():
            case True:
                match self.target.readlink() == self.source:
                    case True:
                        logging.info(f"Deleting Symlink: {Symlink.format_symlink_string(self.target, self.source)}")
                        self.target.unlink()
                    case False:
                        logging.debug(f"Not deleting Symlink because it points somewhere else")
                        logging.debug(f"  expected: {Symlink.format_symlink_string(self.target, self.source)}")
                        logging.debug(f"  found: {Symlink.format_symlink_string(self.target, self.target.readlink())}")
            case False:
                logging.debug(f"Dont need to delete Symlink since there is no Symlink installed: '{self.target}'")

    def check(self) -> None:
        match self.target.is_symlink():
            case True:
                match self.target.readlink() == self.source:
                    case True:
                        logging.info(f"Symlink is installed: {Symlink.format_symlink_string(self.target, self.source)}")
                    case False:
                        logging.info(f"Symlink points somewhere else: {Symlink.format_symlink_string(self.target, self.source)}")
            case False:
                match self.target.exists():
                    case True:
                        logging.info(f"Not a Symlink: '{self.target}'")
                    case False:
                        logging.info(f"Path does not exist: '{self.target}'")


class PackageSymlinkBuilder:
    def __init__(self, package: 'Package') -> None:
        self._package_reference = package
        self._target: Optional[str] = None
        self._source: Optional[str] = None

    def target(self, target: str) -> 'PackageSymlinkBuilder':
        """Set the target path. The Path has to be defined absolute to the Users '$HOME'."""
        assert target.startswith("/")
        self._target = target
        return self

    def source(self, source: str) -> 'PackageSymlinkBuilder':
        """Set the source path. The Path has to be defined absolute to the Packages 'root_path'."""
        assert Path(source).is_absolute()
        self._source = source
        return self

    def build(self) -> None:
        assert self._target, "PackageSymlinkBuilder: 'target' may not be empty"
        assert self._source, "PackageSymlinkBuilder: 'source' may not be empty"
        self._package_reference.build_symlink(Path(self._target), Path(self._source))


class Package:
    def __init__(self, name: str, root: str):
        assert name, f"Package 'name' cant be empty"
        assert root, f"'{name}': Package 'root' cant be empty"
        assert Path(root).exists(), f"'{name}': Root Path does not exist: '{root}'"
        self._name = name
        self._root = root
        self._symlinks: List[Symlink] = []

    def create_symlink(self) -> PackageSymlinkBuilder:
        return PackageSymlinkBuilder(self)

    def build_symlink(self, target: Path, source: Path) -> None:
        """Add a symlink to the package description"""
        assert target.is_absolute(), f"'{self._name}': Target Path has to start with '/': '{target}'"
        assert str(source).startswith(str(self._root)), f"'{self._name}': Source Path has to start with '{self._root}': '{source}'"
        assert source.is_absolute(), f"'{self._name}': Source Path has to start with '/': '{source}'"
        assert source.exists(), f"'{self._name}': Source Path does not exist: '/{source}'"

        self._symlinks.append(Symlink(Path(target), Path(source)))

    def apply(self) -> None:
        """Apply the package state to the system"""
        for symlink in self._symlinks:
            symlink.install()

    def delete(self) -> None:
        """Delete the package state to the system"""
        for symlink in self._symlinks:
            symlink.delete()

    def check(self) -> None:
        """Check all Symlinks if they are registered in the System"""
        for symlink in self._symlinks:
            symlink.check()

    def path(self) -> Path:
        return Path(self._root)


class PackageBuilder:
    def __init__(self) -> None:
        self._name: Optional[str] = None
        self._root: Optional[str] = None

    def name(self, name: str) -> 'PackageBuilder':
        self._name = name
        return self

    def root_path(self, root: str) -> 'PackageBuilder':
        self._root = root
        return self

    def build(self) -> Package:
        assert self._name, "PackageBuilder: 'name' may not be empty"
        assert self._root, "PackageBuilder: 'root' may not be empty"
        package = Package(self._name, self._root)
        PackageStore.instance().add_package(package)
        return package


class PackageStore:
    __instance: Optional['PackageStore'] = None
    def __init__(self) -> None:
        self.packages: List[Package] = []
        pass

    @staticmethod
    def instance() -> 'PackageStore':
        if PackageStore.__instance is None:
            PackageStore.__instance = PackageStore()
        return PackageStore.__instance

    def create_package(self) -> PackageBuilder:
        return PackageBuilder()

    def add_package(self, package: Package) -> None:
        self.packages.append(package)

    def print(self) -> None:
        for package in self.packages:
            print(f"== {package._name} ==")
            print(f"root_path: '{os.path.abspath(package._root)}'")
            if len(package._symlinks) > 0:
                print(f"{len(package._symlinks)} Symlinks:")
                for symlink in package._symlinks:
                    print(f"  {Symlink.format_symlink_string(symlink.target, symlink.source)}")
            else:
                print("0 Symlinks")
            print()

    def apply(self) -> None:
        for package in self.packages:
            package.apply()

    def delete(self) -> None:
        for package in self.packages:
            package.delete()

    def check(self) -> None:
        for package in self.packages:
            package.check()

    def path(self) -> Path:
        return Path(SCRIPT_PATH)


class CliInterface:
    def __init__(self, package_store: PackageStore) -> None:
        self.package_store = package_store
        pass

    def run(self) -> None:
        if len(sys.argv) <= 1:
            self.cmd_help()
            return
        command = sys.argv[1]
        match command:
            case "help":
                self.cmd_help()
            case "apply":
                self.cmd_apply()
            case "delete":
                self.cmd_delete()
            case "check":
                self.cmd_check()
            case "describe":
                self.cmd_describe()
            case "list":
                self.cmd_list()
            case _:
                print(f"unknown command: '{command}'")
                print("---")
                self.cmd_help()

    def cmd_apply(self) -> None:
        self.package_store.apply()

    def cmd_delete(self) -> None:
        self.package_store.delete()

    def cmd_help(self) -> None:
        print("Commands:")
        print("  apply       Apply all package configs (create Symlinks)")
        print("  check       Scan the system for each configured package")
        print("  delete      Delete all package configs (delete existing Symlinks)")
        print("  describe    Dump the full configuration and all registered Symlinks")
        print("  help        Display help menu")
        print("  list        List all Package Names")

    def cmd_describe(self) -> None:
        self.package_store.print()

    def cmd_list(self) -> None:
        for package in self.package_store.packages:
            print(package._name)

    def cmd_check(self) -> None:
        self.package_store.check()
