# If not running interactively, don't do anything
[[ $- != *i* ]] && return

#####################
# +---------------+ #
# | bash settings | #
# +---------------+ #
#####################
# Disable terminal pause using ctrl-s and ctrl-q
stty -ixon
# History settings
HISTCONTROL=ignoredups:erasedups
HISTSIZE=HISTFILESIZE= # Infinite History
shopt -s histappend
# export TERM=xterm-256color

##################
# +------------+ #
# | Clean Path | #
# +------------+ #
##################
if [ -n "$PATH" ]; then
  old_PATH=$PATH:; PATH=
  while [ -n "$old_PATH" ]; do
    x=${old_PATH%%:*}       # the first remaining entry
    case $PATH: in
      *:"$x":*) ;;          # already there
      *) PATH=$PATH:$x;;    # not there yet
    esac
    old_PATH=${old_PATH#*:}
  done
  PATH=${PATH#:}
  unset old_PATH x
fi

###############
# +---------+ #
# | aliases | #
# +---------+ #
###############
if [ -x "/usr/bin/bat" ]; then alias bat='bat --theme Dracula --paging never'; fi
if [ -x "/usr/bin/mpv" ]; then alias mpv='mpv --loop-file=inf'; fi
alias dd='dd status=progress'
alias ll='ls --color=auto --human-readable --indicator-style=slash --group-directories-first --literal --almost-all -l'
alias ls='ls --color=auto --human-readable --indicator-style=slash --group-directories-first --literal'
alias gg='jeeves update-system'
alias code="code --disable-gpu"

####################
# +--------------+ #
# | prompt theme | #
# +--------------+ #
####################
eval "$(starship init bash)"

if [ -d "${HOME}/.brew" ]; then
    ## ```sh
    ## # Archlinux
    ## sudo pacman -S base-devel procps-ng curl file git
    ##
    ## # Debian/Ubuntu
    ## sudo apt-get install build-essential procps curl file git
    ##
    ## export HOMEBREW_NO_ANALYTICS="1"
    ## git clone "https://github.com/Homebrew/brew" "${HOME}/.brew"
    ## eval "$(${HOME}/.brew/bin/brew shellenv)"
    ## brew update
    ## ```
    export HOMEBREW_NO_ANALYTICS="1"
    export HOMEBREW_PREFIX="$HOME/.brew";
    export HOMEBREW_CELLAR="$HOME/.brew/Cellar";
    export HOMEBREW_REPOSITORY="$HOME/.brew";
    fpath[1,0]="$HOME/.brew/share/zsh/site-functions";
    [ -z "${MANPATH-}" ] || export MANPATH=":${MANPATH#:}";
    export INFOPATH="$HOME/.brew/share/info:${INFOPATH:-}";
    if [[ -r "${HOMEBREW_PREFIX}/etc/profile.d/bash_completion.sh" ]]; then
      source "${HOMEBREW_PREFIX}/etc/profile.d/bash_completion.sh"
    else
      for COMPLETION in "${HOMEBREW_PREFIX}/etc/bash_completion.d/"*; do
        [[ -r "${COMPLETION}" ]] && source "${COMPLETION}"
      done
    fi

    ## ```sh
    ## brew install asdf
    ## ```
    if [ -f "${HOMEBREW_PREFIX}/opt/asdf/libexec/asdf.sh" ]; then
        . "${HOMEBREW_PREFIX}/opt/asdf/libexec/asdf.sh"
    fi
fi

###############
# +---------+ #
# | Plugins | #
# +---------+ #
###############

if [ ! -z "$(which zoxide)" ]; then
	eval "$(zoxide init bash)"
else
	printf "Plugin 'zoxide' is not installed.\n"
fi
if [ ! -z "$(which fzf)" ]; then
	eval "$(fzf --bash)"
elif [ -r "/usr/share/fzf/key-bindings.bash" ] && [ -r "/usr/share/fzf/completion.bash" ]; then
	export FZF_DEFAULT_COMMAND='fd --type f --hidden --follow --exclude .git'
	source /usr/share/fzf/key-bindings.bash
	source /usr/share/fzf/completion.bash
else
	printf "Plugin 'fzf' is not installed.\n"
fi
