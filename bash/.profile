############
# +------+ #
# | PATH | #
# +------+ #
############

PATH="${PATH}:${HOME}/.brew/bin"
PATH="${PATH}:${HOME}/.brew/sbin"

PATH="${HOME}/.bin:${PATH}"
PATH="${HOME}/.local/bin:${PATH}"
PATH="${HOME}/.local/share/bin:${PATH}"
PATH="${HOME}/.composer/vendor/bin:${PATH}"
PATH="${HOME}/.krew/bin:${PATH}"
PATH="${HOME}/.zvm/bin:${PATH}"
PATH="${HOME}/go/bin:${PATH}"
PATH="${HOME}/.cargo/bin:${PATH}"
PATH="${HOME}/.asdf/bin:${PATH}"

export PATH
