return {
    "tpope/vim-fugitive",
    event = "VimEnter",
    config = function(_, _)
        vim.keymap.set("n", "<leader>gs", vim.cmd.Git)
    end,
}
