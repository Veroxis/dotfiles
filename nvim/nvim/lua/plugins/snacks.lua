return {
    "folke/snacks.nvim",
    priority = 1000,
    lazy = false,
    config = function()
        local snacks = require("snacks")
        ---@type snacks.Config
        ---@diagnostic disable-next-line: missing-fields
        local opts = {
            bigfile = {
                notify = true,
                size = 10.0 * 1024 * 1024,
                enabled = true,
            },
            dashboard = { enabled = false },
            indent = { enabled = false },
            input = { enabled = true },
            ---@class snacks.notifier.Config
            notifier = {
                enabled = true,
                style = "fancy",
            },
            quickfile = { enabled = true },
            scroll = { enabled = false },
            statuscolumn = { enabled = false },
            words = { enabled = false },
        }
        snacks.setup(opts)
    end
}
