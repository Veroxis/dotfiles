return {
    "catppuccin/nvim",
    name = "catppuccin",
    event = "VimEnter",
    priority = 1000,
    lazy = false,
    opts = {},
    config = function(_, _)
        vim.cmd.colorscheme("catppuccin-mocha")
    end,
}
