return {
    "nvim-treesitter/nvim-treesitter-context",
    event = "VimEnter",
    config = function()
        local tsconfig = require("treesitter-context")

        tsconfig.setup({
            max_lines = 3,
        })
    end,
}
