return {
    "numToStr/Comment.nvim",
    event = "VimEnter",
    lazy = false,
    opts = {},
    config = function(_, opts)
        require("Comment").setup(opts)
        vim.keymap.set("n", "<C-c>", function()
            require("Comment.api").toggle.linewise.current()
        end, { desc = "Comment a single line in normal mode" })

        vim.keymap.set("x", "<C-c>", function()
            local esc = vim.api.nvim_replace_termcodes("<ESC>", true, false, true)
            vim.api.nvim_feedkeys(esc, "nx", false)
            require("Comment.api").toggle.linewise(vim.fn.visualmode())
        end, { desc = "Comment selected lines in visual mode" })
    end,
}
