return {
    "nvim-lualine/lualine.nvim",
    event = "VimEnter",
    dependencies = {
        "kyazdani42/nvim-web-devicons",
        "folke/tokyonight.nvim",
        "nvim-lua/lsp-status.nvim",
    },
    config = function(_, _)
        local function lsp_status()
            local prefix_icon = "\u{f085}  "

            local clients = vim.lsp.get_clients()

            if next(clients) == nil then
                return prefix_icon .. "nil"
            end

            local bufnr = vim.api.nvim_get_current_buf()
            local clients_in_current_buf = {}
            for _, client in pairs(clients) do
                if client.attached_buffers[bufnr] == true then
                    table.insert(clients_in_current_buf, client)
                end
            end

            if next(clients_in_current_buf) == nil then
                return prefix_icon .. "nil"
            end

            local client_names_current_buf = {}
            for _, client in pairs(clients_in_current_buf) do
                table.insert(client_names_current_buf, client.name)
            end

            return prefix_icon .. table.concat(client_names_current_buf, "|")
        end

        require("lualine").setup({
            sections = {
                lualine_a = { "mode" },
                lualine_b = {
                    {
                        "filename",
                        path = 1,
                    },
                },
                lualine_c = {
                    lsp_status,
                },
            },
        })
    end,
}
