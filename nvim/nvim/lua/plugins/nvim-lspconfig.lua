return {
    -- LSP Configuration & Plugins
    "neovim/nvim-lspconfig",
    dependencies = {
        -- Automatically install LSPs and related tools to stdpath for neovim
        "williamboman/mason.nvim",
        "williamboman/mason-lspconfig.nvim",
        "WhoIsSethDaniel/mason-tool-installer.nvim",

        -- Useful status updates for LSP.
        -- NOTE: `opts = {}` is the same as calling `require('fidget').setup({})`
        { "j-hui/fidget.nvim", opts = {} },

        -- `neodev` configures Lua LSP for your Neovim config, runtime and plugins
        -- used for completion, annotations and signatures of Neovim apis
        { "folke/neodev.nvim", opts = {} },
    },
    config = function(_, _)
        --  This function gets run when an LSP attaches to a particular buffer.
        --    That is to say, every time a new file is opened that is associated with
        --    an lsp (for example, opening `main.rs` is associated with `rust_analyzer`) this
        --    function will be executed to configure the current buffer
        vim.api.nvim_create_autocmd("LspAttach", {
            group = vim.api.nvim_create_augroup("lsp-attach", { clear = true }),
            callback = function(event)
                -- NOTE: Remember that lua is a real programming language, and as such it is possible
                -- to define small helper and utility functions so you don't have to repeat yourself
                -- many times.
                --
                -- In this case, we create a function that lets us more easily define mappings specific
                -- for LSP related items. It sets the mode, buffer and description for us each time.
                local map = function(keys, func, desc)
                    vim.keymap.set("n", keys, func, { buffer = event.buf, desc = "LSP: " .. desc })
                end

                -- create a command `:Format` local to the LSP buffer
                vim.api.nvim_buf_create_user_command(event.buf, "Format", function(_)
                    vim.lsp.buf.format()
                    vim.diagnostic.show()
                end, { desc = "Format current buffer with LSP" })

                map("F", function()
                    vim.lsp.buf.format()
                    vim.diagnostic.show()
                end, "[F]ormat current buffer with LSP")

                -- See `:help K` for why this keymap
                map("<C-k>", vim.lsp.buf.signature_help, "Signature Documentation")

                map("<leader>q", require("telescope.builtin").diagnostics, "Open diagnosics [Q]ickfix list")
                -- map("<leader>q", vim.diagnostic.setloclist, "Open diagnosics [Q]ickfix list")

                -- Lesser used LSP functionality
                map("gD", vim.lsp.buf.declaration, "[G]oto [D]eclaration")
                map("<leader>wa", vim.lsp.buf.add_workspace_folder, "[W]orkspace [A]dd Folder")
                map("<leader>wr", vim.lsp.buf.remove_workspace_folder, "[W]orkspace [R]emove Folder")
                map("<leader>wl", function()
                    print(vim.inspect(vim.lsp.buf.list_workspace_folders()))
                end, "[W]orkspace [L]ist Folders")

                -- Jump to the definition of the word under your cursor.
                --  This is where a variable was first declared, or where a function is defined, etc.
                --  To jump back, press <C-t>.
                map("gd", require("telescope.builtin").lsp_definitions, "[G]oto [D]efinition")

                -- Find references for the word under your cursor.
                map("gr", require("telescope.builtin").lsp_references, "[G]oto [R]eferences")

                -- Jump to the implementation of the word under your cursor.
                --  Useful when your language has ways of declaring types without an actual implementation.
                map("gI", require("telescope.builtin").lsp_implementations, "[G]oto [I]mplementation")

                -- Jump to the type of the word under your cursor.
                --  Useful when you're not sure what type a variable is and you want to see
                --  the definition of its *type*, not where it was *defined*.
                map("<leader>D", require("telescope.builtin").lsp_type_definitions, "Type [D]efinition")

                -- Fuzzy find all the symbols in your current document.
                --  Symbols are things like variables, functions, types, etc.
                map("<leader>ds", require("telescope.builtin").lsp_document_symbols, "[D]ocument [S]ymbols")

                -- Fuzzy find all the symbols in your current workspace
                --  Similar to document symbols, except searches over your whole project.
                map("<leader>ws", require("telescope.builtin").lsp_dynamic_workspace_symbols, "[W]orkspace [S]ymbols")

                -- Rename the variable under your cursor
                --  Most Language Servers support renaming across files, etc.
                map("<leader>rn", vim.lsp.buf.rename, "[R]e[n]ame")

                -- Execute a code action, usually your cursor needs to be on top of an error
                -- or a suggestion from your LSP for this to activate.
                map("<leader>ca", vim.lsp.buf.code_action, "[C]ode [A]ction")

                -- Opens a popup that displays documentation about the word under your cursor
                --  See `:help K` for why this keymap
                map("K", function()
                    vim.lsp.buf.hover()
                end, "Hover Documentation")

                -- WARN: This is not Goto Definition, this is Goto Declaration.
                --  For example, in C this would take you to the header
                map("gD", vim.lsp.buf.declaration, "[G]oto [D]eclaration")

                -- The following two autocommands are used to highlight references of the
                -- word under your cursor when your cursor rests there for a little while.
                --    See `:help CursorHold` for information about when this is executed
                --
                -- When you move your cursor, the highlights will be cleared (the second autocommand).
                local client = vim.lsp.get_client_by_id(event.data.client_id)
                if not client then return end

                -- format of save
                -- if client.supports_method("textDocument/formatting") then
                --     vim.api.nvim_create_autocmd("BufWritePre", {
                --         buffer = event.buf,
                --         callback = function()
                --             vim.lsp.buf.format({ bufnr = event.buf, id = client.id })
                --             vim.diagnostic.show()
                --         end
                --     })
                -- end
                if client.server_capabilities.documentHighlightProvider then
                    vim.api.nvim_create_autocmd({ "CursorHold", "CursorHoldI" }, {
                        buffer = event.buf,
                        callback = vim.lsp.buf.document_highlight,
                    })

                    vim.api.nvim_create_autocmd({ "CursorMoved", "CursorMovedI" }, {
                        buffer = event.buf,
                        callback = vim.lsp.buf.clear_references,
                    })
                end
            end,
        })
        vim.lsp.inlay_hint.enable(true)

        -- Enable the following language servers
        --  Feel free to add/remove any LSPs that you want here. They will automatically be installed.
        --
        --  Add any additional override configuration in the following tables. They will be passed to
        --  the `settings` field of the server config. You must look up that documentation yourself.
        --
        --  If you want to override the default filetypes that your language server will attach to you can
        --  define the property "filetypes" to the map in question.
        --
        -- https://github.com/neovim/nvim-lspconfig/blob/master/doc/server_configurations.md
        local servers = {}

        -- To install LSPs sometimes executables are required (e.g. curl, git, npm, pip).
        --  For compatibility across systems LSPs are only installed if these dependencies are available.
        local function add_lsp_if_executables_exist(executables, lsp_config)
            -- return from the method if any `executable` does not exist
            for _, executable in ipairs(executables) do
                if vim.fn.executable(executable) ~= 1 then
                    return nil
                end
            end
            -- add the `lsp_config` to the `servers` list
            servers = vim.tbl_deep_extend("force", servers, lsp_config)
        end

        -- LUA LSP
        add_lsp_if_executables_exist({}, {
            lua_ls = {
                settings = {
                    Lua = {
                        hint = {
                            enable = true,
                        },
                        workspace = {
                            checkThirdParty = false,
                        },
                        telemetry = {
                            enable = false,
                        },
                    },
                },
            },
        })

        -- Markdown LSP
        add_lsp_if_executables_exist({}, { marksman = {} })

        -- ZIG LSP
        add_lsp_if_executables_exist({}, { zls = {} })

        -- C, C++ LSP
        add_lsp_if_executables_exist({}, { clangd = {} })

        -- BASH LSP
        add_lsp_if_executables_exist({ "npm" }, { bashls = {} })

        -- Nix LSP
        add_lsp_if_executables_exist({ "cargo", "nix" }, { nil_ls = {} })

        -- Python LSP
        add_lsp_if_executables_exist({ "npm" }, {
            pyright = {
                single_file_support = true,
                settings = {
                    -- pyright = {
                    --     disableLanguageServices = true,
                    --     disableOrganizeImports = true,
                    -- },
                    python = {
                        analysis = {
                            autoImportCompletions = true,
                            autoSearchPaths = true,
                            diagnosticMode = "openFilesOnly", -- openFilesOnly, workspace
                            typeCheckingMode = "strict",      -- off, basic, strict
                            useLibraryCodeForTypes = true,
                        },
                    },
                },
            }
        })

        -- WGSL LSP
        add_lsp_if_executables_exist({}, { wgsl_analyzer = {} })

        -- Golang LSP
        add_lsp_if_executables_exist({ "go" }, {
            gopls = {
                settings = {
                    ["gopls"] = {
                        completeUnimported = true,
                        usePlaceholders = true,
                        analyses = {
                            unusedparams = true,
                        },
                        hints = {
                            assignVariableTypes = true,
                            compositeLiteralFields = true,
                            compositeLiteralTypes = true,
                            constantValues = true,
                            functionTypeParameters = true,
                            parameterNames = true,
                            rangeVariableTypes = true,
                        },
                    },
                },
            },
        })

        -- OpenAPI LSP
        add_lsp_if_executables_exist({ "npm" }, { spectral = {} })

        -- JQ LSP
        add_lsp_if_executables_exist({ "go" }, { jqls = {} })

        -- SQL LSP
        add_lsp_if_executables_exist({ "npm" }, { sqlls = {} })

        -- HTML LSP
        add_lsp_if_executables_exist({ "npm" }, {
            html = {
                filetypes = { "html", "twig", "hbs" },
            },
        })

        -- Javascript and Typescript LSP
        add_lsp_if_executables_exist({ "npm" }, {
            ts_ls = {
                -- https://github.com/typescript-language-server/typescript-language-server/blob/master/docs/configuration.md
                init_options = {
                    hostInfo = "neovim",
                    preferences = {
                        includeInlayParameterNameHints = "all",
                        includeInlayParameterNameHintsWhenArgumentMatchesName = true,
                        includeInlayFunctionParameterTypeHints = true,
                        includeInlayVariableTypeHints = true,
                        includeInlayVariableTypeHintsWhenTypeMatchesName = true,
                        includeInlayPropertyDeclarationTypeHints = true,
                        includeInlayFunctionLikeReturnTypeHints = true,
                        includeInlayEnumMemberValueHints = true,
                        -- Preferred path style for auto imports.
                        -- Options: "shortest", "project-relative", relative", "non-relative"
                        -- Default: "shortest"
                        importModuleSpecifierPreference = "non-relative",
                    },
                },
            },
        })

        -- PHP LSP
        add_lsp_if_executables_exist({ "composer" }, {
            phpactor = {
                -- https://phpactor.readthedocs.io/en/master/reference/configuration.html#ref-configuration
                init_options = {
                    ["language_server_phpstan.enabled"] = true,
                    ["language_server_psalm.enabled"] = true,
                    ["language_server_worse_reflection.inlay_hints.enable"] = true,
                    ["language_server_worse_reflection.inlay_hints.params"] = true,
                    ["language_server_worse_reflection.inlay_hints.types"] = false,
                },
            },
        })

        -- Java LSP
        add_lsp_if_executables_exist({}, {
            jdtls = {
                cmd = { "jdtls" },
            },
        })

        -- Kotlin LSP
        add_lsp_if_executables_exist({}, {
            kotlin_language_server = {
                -- https://github.com/fwcd/kotlin-language-server
                cmd = { "kotlin-language-server" },
                filetypes = { "kotlin" },
            },
        })

        -- TOML LSP
        add_lsp_if_executables_exist({}, { taplo = {} })

        -- JSON LSP
        add_lsp_if_executables_exist({ "npm" }, { jsonls = {} })

        -- YAML LSP
        add_lsp_if_executables_exist({ "npm" }, { yamlls = {} })

        -- XML LSP
        add_lsp_if_executables_exist({}, { lemminx = {} })

        -- Docker LSP
        add_lsp_if_executables_exist({ "npm" }, { dockerls = {} })

        -- Docker Composer LSP
        add_lsp_if_executables_exist({ "npm" }, { docker_compose_language_service = {} })

        -- Terraform LSP
        add_lsp_if_executables_exist({}, { terraformls = {} })

        -- Setup neovim lua configuration
        require("neodev").setup()

        -- Ensure the servers and tools above are installed
        --  To check the current status of installed tools and/or manually install
        --  other tools, you can run
        --    :Mason
        --
        --  You can press `g?` for help in this menu
        require("mason").setup()

        -- LSP servers and clients are able to communicate to each other what features they support.
        --  By default, Neovim doesn't support everything that is in the LSP Specification.
        --  When you add nvim-cmp, luasnip, etc. Neovim now has *more* capabilities.
        --  So, we create new capabilities with nvim cmp, and then broadcast that to the servers.
        --- @type lsp.ClientCapabilities
        local capabilities = vim.tbl_deep_extend("force",
            vim.lsp.protocol.make_client_capabilities(),
            require("cmp_nvim_lsp").default_capabilities()
        )

        require("mason-lspconfig").setup({
            automatic_installation = true,
            ensure_installed = vim.tbl_keys(servers),
            handlers = {
                function(server_name)
                    local server = servers[server_name] or {}
                    --- @type lsp.ClientCapabilities
                    server.capabilities = vim.tbl_deep_extend("force", {}, capabilities, server.capabilities or {})
                    require("lspconfig")[server_name].setup(server)
                end,
            },
        })
    end,
}
