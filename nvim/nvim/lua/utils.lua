P = function(v)
    vim.notify(vim.inspect(v))
    return v
end

RELOAD = function(...)
    return require("plenary.reload").reload_module(...)
end

R = function(name)
    RELOAD(name)
    return require(name)
end

local M = {}

function M.print(v)
    return P(v)
end

function M.visible_dirs_in_path(path)
    local detected_dirs = {}

    local handle = vim.loop.fs_scandir(path)
    if not handle then
        return {}
    end

    while true do
        local name, type = vim.loop.fs_scandir_next(handle)
        if not name then
            break
        end

        if type == "directory" and name ~= "." and name ~= ".." then
            if name:sub(1, 1) ~= "." then
                table.insert(detected_dirs, name)
            end
        end
    end
    return detected_dirs
end

return M
