local command_id = 0

local menu_declare_action = function(title, action)
    if type(action) == "string" then
        vim.cmd([[anoremenu PopUp.]] .. title .. " " .. action)
    elseif type(action) == "function" then
        command_id = command_id + 1
        local command_name = "XGeneratedContextCommand" .. command_id
        vim.api.nvim_create_user_command(command_name, action,
            { desc = "Generated command for Context Menu with ID=" .. command_id })
        vim.cmd([[anoremenu PopUp.]] .. title .. " " .. "<cmd>" .. command_name .. "<CR>")
    end
end

vim.cmd([[aunmenu PopUp]])
menu_declare_action([[Inspect]], "<cmd>Inspect<CR>")
menu_declare_action([[Inspect\ Tree]], "<cmd>InspectTree<CR>")
menu_declare_action([[Back]], "<C-t>")
menu_declare_action([[Close\ Buffer]], "<cmd>BufferClose<CR>")
menu_declare_action([[Close\ Window]], function() vim.api.nvim_win_close(0, true) end)
menu_declare_action([[LSP:\ Definition]], function() vim.lsp.buf.definition() end)
menu_declare_action([[LSP:\ References]], "<cmd>Telescope lsp_references<CR>")
menu_declare_action([[DAP:\ Start/Continue]], function()
    require("dap").continue()
end)
menu_declare_action([[DAP:\ Toggle\ Breakpoint]], function() require("dap").toggle_breakpoint() end)
menu_declare_action([[DAP:\ Clear\ Breakpoints]], function() require("dap").clear_breakpoints() end)
