-- Highlight when yanking (copying) text
--  Try it with `yap` in normal mode
--  See `:help vim.highlight.on_yank()`
vim.api.nvim_create_autocmd("TextYankPost", {
    desc = "Highlight when yanking (copying) text",
    group = vim.api.nvim_create_augroup("kickstart-highlight-yank", { clear = true }),
    callback = function()
        vim.highlight.on_yank()
    end,
})

-- vim.api.nvim_create_autocmd({ "TermOpen", "BufEnter" }, {
--     group = vim.api.nvim_create_augroup('terminal-open-insert', { clear = true }),
--     pattern = { "*" },
--     callback = function()
--         if vim.opt.buftype:get() == "terminal" then
--             vim.cmd(":startinsert")
--         end
--     end
-- })

local tmux_session = vim.fn.getenv("TMUX_PANE") ~= vim.NIL
local function set_tmux_pane_title()
    local dirname = vim.fn.fnamemodify(vim.fn.getcwd(), ":t")
    local command = string.format("tmux rename-window 'nvim {%s}'", dirname)
    vim.fn.system(command)
end

if tmux_session then
    -- On Start
    set_tmux_pane_title()

    -- On Dir Change
    vim.api.nvim_create_autocmd("DirChanged", {
        desc = "Set the tmux pane title to the current directory name",
        group = vim.api.nvim_create_augroup("tmux", { clear = false }),
        callback = function()
            set_tmux_pane_title()
        end,
    })

    -- On Close
    vim.api.nvim_create_autocmd("ExitPre", {
        desc = "Reset the tmux pane title on exit",
        group = vim.api.nvim_create_augroup("tmux", { clear = false }),
        callback = function()
            vim.fn.system("tmux setw automatic-rename")
        end,
    })
end
