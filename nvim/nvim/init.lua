-- https://github.com/neovim/neovim/issues/31675#issuecomment-2558405042
vim.hl = vim.highlight

require("sets")
require("utils")
require("autocommands")

require("init_lazy")

require("keymaps")
require("contextmenu")
require("telescope_finders")
