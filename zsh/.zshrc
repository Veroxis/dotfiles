###################
# +-------------+ #
# | base config | #
# +-------------+ #
###################
PROMPT='%F{cyan}%n%f@%F{blue}%m%f %F{green}%~%f> '    # Prompt: a basic default
HISTFILE="${HOME}/.zsh_history"                       # History: location for file
HISTSIZE=10000000                                     # History: maximum size in memory
SAVEHIST=10000000                                     # History: maximum size in file
export HISTCONTROL=ignorespace                        # History: configure how entries are stored
setopt BANG_HIST                                      # History: Treat the '!' character specially during expansion.
setopt EXTENDED_HISTORY                               # History: Write the history file in the ":start:elapsed;command" format.
setopt INC_APPEND_HISTORY                             # History: Write to the history file immediately, not when the shell exits.
setopt SHARE_HISTORY                                  # History: Share history between all sessions.
setopt HIST_EXPIRE_DUPS_FIRST                         # History: Expire duplicate entries first when trimming history.
setopt HIST_IGNORE_DUPS                               # History: Don't record an entry that was just recorded again.
setopt HIST_IGNORE_ALL_DUPS                           # History: Delete old recorded entry if new entry is a duplicate.
setopt HIST_FIND_NO_DUPS                              # History: Do not display a line previously found.
setopt HIST_IGNORE_SPACE                              # History: Don't record an entry starting with a space.
setopt HIST_SAVE_NO_DUPS                              # History: Don't write duplicate entries in the history file.
setopt HIST_REDUCE_BLANKS                             # History: Remove superfluous blanks before recording entry.
setopt HIST_VERIFY                                    # History: Don't execute immediately upon history expansion.
stty -ixon                                            # Shortcuts: disable ctrl+s and ctrl+q
bindkey "\e[3~" delete-char                           # Shortcuts: <Del>: delete next character
bindkey "^[[3;5~" kill-word                           # Shortcuts: <CTRL + Del>: kill next word
bindkey "^H" backward-kill-word                       # Shortcuts: <CTRL + Backspace>: kill previous word
bindkey "^[[1;5C" forward-word                        # Shortcuts: <CTRL + ArrowRight>: jump over words when navigating the cursor
bindkey "^[[1;5D" backward-word                       # Shortcuts: <CTRL + ArrowLeft>: jump over words when navigating the cursor
source ~/.config/zsh/env.zsh                          # custom functions
source ~/.config/zsh/utils.zsh                        # custom functions
source ~/.config/zsh/aliases.zsh                      # custom functions

##################################
# +----------------------------+ #
# | source all from zsh config | #
# +----------------------------+ #
##################################

for file in ~/.config/zsh/plugins/*.zsh; do
    source "$file"
done

#############################
# +-----------------------+ #
# | finish initialization | #
# +-----------------------+ #
#############################

typeset -U path
autoload -Uz compinit
compinit
