cmd_exists() {
    # using this zsh inbuilt is the fastest way to lookup executables
    if [ $+commands[${1}] -eq 1 ]; then
        return 0
    else
        # since the previous check won't catch freshly installed packages
        # the `command` will serve as a fallback
        command -v $1 2>&1 > /dev/null
        CMD_EXISTS=${?}
        if [ $CMD_EXISTS -eq 0 ]; then
            return 0
        else
            return 1
        fi
    fi
}
