if cmd_exists "dd"; then
    alias dd="dd status=progress"
fi

if cmd_exists "bat"; then
    alias bat="bat --theme Dracula --paging never"
fi

if cmd_exists "mpv"; then
    alias mpv="mpv --loop-file=inf"
fi

if cmd_exists "nvim"; then
    alias vim="nvim"
    alias less="nvim -R -"
    export EDITOR="nvim"
fi

if cmd_exists "eza"; then
    alias ls="eza --group --follow-symlinks --mounts --hyperlink"
fi

if cmd_exists "gio"; then
    alias trash="gio trash"
fi

if [ "$TERM" = "xterm-kitty" ]; then
    alias ssh="kitten ssh"
fi
