if [ -d "${HOME}/.brew" ]; then
    ## ```sh
    ## # Archlinux
    ## sudo pacman -S base-devel procps-ng curl file git
    ##
    ## # Debian/Ubuntu
    ## sudo apt-get install build-essential procps curl file git
    ##
    ## export HOMEBREW_NO_ANALYTICS="1"
    ## git clone "https://github.com/Homebrew/brew" "${HOME}/.brew"
    ## eval "$(${HOME}/.brew/bin/brew shellenv)"
    ## brew update
    ## ```
    export HOMEBREW_NO_ANALYTICS="1"
    export HOMEBREW_PREFIX="$HOME/.brew";
    export HOMEBREW_CELLAR="$HOME/.brew/Cellar";
    export HOMEBREW_REPOSITORY="$HOME/.brew";
    fpath[1,0]="$HOME/.brew/share/zsh/site-functions";
    [ -z "${MANPATH-}" ] || export MANPATH=":${MANPATH#:}";
    FPATH="${HOMEBREW_PREFIX}/share/zsh/site-functions:${FPATH}"

    ## ```sh
    ## brew install asdf
    ## ```
    if [ -f "${HOMEBREW_PREFIX}/opt/asdf/libexec/asdf.sh" ]; then
        . "${HOMEBREW_PREFIX}/opt/asdf/libexec/asdf.sh"
    fi
fi
