if cmd_exists asdf; then
    export PATH="${ASDF_DATA_DIR:-$HOME/.asdf}/shims:$PATH"
    fpath=(${ASDF_DATA_DIR:-$HOME/.asdf}/completions $fpath)
fi
