# cargo binstall --no-confirm sheldon
if cmd_exists "sheldon"; then
    export SHELDON_CONFIG_FILE="${HOME}/.config/sheldon/plugins-zsh.toml"
    eval "$(sheldon source)"
fi
