#!/usr/bin/env python3

import sys
import subprocess
from shutil import which

PS_MEM = "ps_mem"
PGREP = "pgrep"

def main() -> int:
    if len(sys.argv) == 1:
        print("mem_usage")
        print("print ram usage of running processes by name")
        print()
        print("USAGE:")
        print("    mem_usage <process_names>")
    if not required_executables_are_installed():
        return 1
    processes: list[ProcessInfo] = []
    for i in range(1, len(sys.argv)):
        arg = sys.argv[i]
        pids = get_pids(arg)
        if len(pids) == 0:
            print(f"WARNING: found zero processes matching `{arg}`", file = sys.stderr)
            continue
        for pid in pids:
            usage = mem_usage_of_pid(pid)
            processes.append(ProcessInfo(arg, pid, format_bytes(usage)))
    ProcessInfo.print_process_table(processes)
    return 0

class ProcessInfo:
    def __init__(self, query: str, pid: int, usage: str) -> None:
        self.query = query
        self.pid = pid
        self.usage = usage
        pass

    @staticmethod
    def print_process_table(processes: list['ProcessInfo']) -> None:
        query_len = ProcessInfo.get_max_query_len(processes)
        pid_len = ProcessInfo.get_max_pid_len(processes)
        usage_len = ProcessInfo.get_max_usage_len(processes)
        for p in processes:
            print(f"{p.query.ljust(query_len)} {str(p.pid).rjust(pid_len)} {p.usage.rjust(usage_len)}")

    @staticmethod
    def get_max_query_len(processes: list['ProcessInfo']) -> int:
        longest = 0
        for process in processes:
            if len(process.query) > longest:
                longest = len(process.query)
        return longest

    @staticmethod
    def get_max_pid_len(processes: list['ProcessInfo']) -> int:
        longest = 0
        for process in processes:
            if len(str(process.pid)) > longest:
                longest = len(str(process.pid))
        return longest

    @staticmethod
    def get_max_usage_len(processes: list['ProcessInfo']) -> int:
        longest = 0
        for process in processes:
            if len(process.usage) > longest:
                longest = len(process.usage)
        return longest

def mem_usage_of_pid(pid: int) -> int:
    p = subprocess.Popen([PS_MEM, "-t", "-p", str(pid)], stdout=subprocess.PIPE)
    stdout = p.stdout
    if not stdout:
        return 0
    out = stdout.read().decode('utf-8').strip()
    mem_usage = try_as_int(out)
    if mem_usage:
        return mem_usage
    return 0

def get_pids(name: str) -> list[int]:
    buffer: list[int] = []
    p = subprocess.Popen([PGREP, name], stdout=subprocess.PIPE)
    stdout = p.stdout
    if not stdout:
        return buffer
    out = stdout.read().decode('utf-8')
    for line in out.split('\n'):
        line = line.strip()
        if len(line) > 0:
            pid = try_as_int(line)
            if pid:
                buffer.append(pid)
    return buffer

def required_executables_are_installed() -> bool:
    executables_are_installed = True
    REQUIRED_EXECUTABLES = [
        PS_MEM,
        PGREP,
    ]

    for executable in REQUIRED_EXECUTABLES:
        if not executable_is_available(executable):
            print(f"ERROR: executable missing: `{executable}`")
            executables_are_installed = False

    return executables_are_installed

def executable_is_available(name: str) -> bool:
    return which(name) is not None

def try_as_int(data: str) -> int|None:
    try:
        return int(data)
    except ValueError:
        return None

def format_bytes(size: float) -> str:
    power = 2**10
    n = 0
    power_labels = {0 : '', 1: 'Ki', 2: 'Mi', 3: 'Gi', 4: 'Ti'}
    while size > power:
        size /= power
        n += 1
    return f"{size:.2f}{power_labels[n]+'B'}"

if __name__ == "__main__":
    code = main()
    sys.exit(code)
